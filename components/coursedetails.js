class Coursedetails extends HTMLElement {
    constructor() {
      super();
    }
  
    connectedCallback() {
      this.innerHTML = `
            <style>
            </style>
            
            <div class="mt-6">
            <h4 class="content is-6 has-text-info has-text-weight-bold mb-1">
              DURATION
            </h4>
            <p>
              ⏱️&nbsp;&nbsp;The program is set to run for 4 months.
            </p>
          </div>
          <div class="mt-6">
            <h4 class="content is-6 has-text-info has-text-weight-bold mb-1">
              PRE-REQUISITE
            </h4>
            <p>
              🧑🏾‍💻&nbsp;&nbsp;Successfully complete the assessment.
            </p>
          </div>
          `;
    }
  }
  
  customElements.define("coursedetails-component", Coursedetails);
  