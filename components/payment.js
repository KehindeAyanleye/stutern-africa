class Payment extends HTMLElement {
    constructor() {
      super();
    }
  
    connectedCallback() {
      this.innerHTML = `
            <style>
            </style>
            
            <section class="hero is-medium py-6">
    <div class="hero-body pb-0">
      <div class="container">
        <div class="columns is-multiline is-vcentered">
          <div class="column is-full">
            <h2 class="title subtitle-spacing is-2 is-size-3-mobile is-family-secondary has-text-info">
              Transparent & fair payment options
            </h2>
          </div>
          <div class="column is-7-desktop is-full-tablet">
            <p class="subtitle is-size-6 has-text-weight-light has-text-info">
              Prioritize learning now, and figure out payments later. From
              upfront to installment payments, there are flexible options to
              accommodate multiple circumstances.
            </p>
            <br />

            <div class="columns is-multiline">
              
              <div class="column is-full">
                <div class="box is-shadowless has-background-info payment-box px-5 py-6">
                  <div class="content">
                    <p class="is-family-secondary has-text-primary-light is-size-4 mb-4">
                      Full Tuition Option
                    </p>
                    <p class="is-family-secondary has-text-white is-size-2 subtitle-spacing mt-0 mb-2">
                      ₦500,000
                    </p>
                    <p class="has-text-primary-light">
                      Pay your fees 100% upfront. No other charges come up
                      during your learning.
                    </p>
                  </div>
                </div>
              </div>

              <div class="column is-half">
                <div class="box is-shadowless payment-box px-5 py-6">
                  <div class="content">
                    <p class="is-family-secondary has-text-info is-size-4 mb-4">
                      A-3 Installment Option
                    </p>
                    <p class="has-text-info">
                      Complete your payment within 3 months of the program in 3 installments. ₦175,000 + ₦175,000 +
                      ₦175,000.
                    </p>
                  </div>
                </div>
              </div>

              <div class="column is-half">
                <div class="box is-shadowless payment-box px-5 py-6">
                  <div class="content">
                    <p class="is-family-secondary has-text-info is-size-4 mb-4">
                      A-4 Installment Option
                    </p>
                    <p class="has-text-info">
                      Complete your payment within 4 months of the program in 4 installments. ₦135,000 + ₦135,000 +
                      ₦135,000 + ₦135,000.
                    </p>
                  </div>
                </div>
              </div>

              <div class="column is-1 is-hidden-tablet-only"></div>
            </div>
            
          </div>
          <div class="column is-1 is-hidden-tablet-only"></div>
          <div class="column is-4">
            <figure class="image">
              <img src="/images/accelerator/tuition-intermediate.png" alt="intermediate-tuition-image"
                style="max-width: 100%" class="landing_rocket" />
            </figure>
          </div>
        </div>
      </div>
    </div>
  </section>
          `;
    }
  }
  
  customElements.define("payment-component", Payment);