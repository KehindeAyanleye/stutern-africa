class Update extends HTMLElement {
    constructor() {
      super();
    }
  
    connectedCallback() {
      this.innerHTML = `
            <style>
            </style>
            
            <section class="hero is-small has-background-info">
                <div class="hero-body">
                    <div class="container has-text-centered is-family-secondary has-text-primary">
                    <p> Apply now! &nbsp;  🗓️ Deadline: March 23rd, 2024. ⏰ Early applicants receive priority.</p>
                </div>
                </div>
            </section>
          `;
    }
  }
  
  customElements.define("update-component", Update);
  