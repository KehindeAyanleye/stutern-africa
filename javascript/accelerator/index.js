    // Scroll to the courses section
    const accelerator_track = document.getElementById("accelerator-tracks");
    const apply_buttons = document.getElementsByClassName("apply-button");

    // Accordion
    const questionItems = document.getElementsByClassName("questionItem");

    // Loop through all apply buttons
    for (let i = 0; i < apply_buttons.length; i++) {

        // Scroll to the course section
        apply_buttons[i].onclick = function scrollToApplySection() {

            accelerator_track.scrollIntoView({
                behavior: "auto"
            });
        }
    }

    // Run accordion
    for (let i = 0; i < questionItems.length; i++) {
        const questionItem = questionItems[i];

        questionItem.onclick = function runAccordion() {
            let answerItem = questionItem.nextElementSibling;
            let arrowIcon = questionItem.querySelector(".arrowIcon");

            answerItem.classList.toggle("is-hidden");

            if (answerItem.classList.contains("is-hidden")) {
                arrowIcon.setAttribute('class', 'fas fa-chevron-down arrowIcon');
            } else {
                arrowIcon.setAttribute('class', 'fas fa-chevron-down fa-rotate-90 arrowIcon');
            }
        }
    }